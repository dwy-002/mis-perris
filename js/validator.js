$(function () {



    //e9c7e4860f881f2fa6d870566328a112a22b6508
    $.ajax({
        method: "get",
        url: 'http://chile.daedalus.cl/api/region'
    }).done(function (response) {
        regiones = response.sort(function (a, b) {
            a = a.region_id;
            b = b.region_id;
            return a - b;
        });
        regiones.forEach(function (region) {
            var o = "<option value='" + region.region_id + "'>" + region.region_nombre + "</option>";
            $("#region").append(o);
        });
        $("#region").on("change", regionChange);
    }).fail();



});
function regionChange(e) {
    console.log(e.target.value)
    $.ajax({
        method: "get",
        url: 'http://chile.daedalus.cl/api/comuna/region/' + e.target.value
    }).done(function (response) {
        console.log(response)
        $("#ciudad").removeAttr("disabled");
        comunas = response.sort(function (a, b) {
            a = a.comuna_id;
            b = b.comuna_id;
            return a - b;
        });
        comunas.forEach(function (comuna) {
            var o = "<option value='" + comuna.comuna_id + "'>" + comuna.comuna_nombre + "</option>";
            $("#ciudad").append(o);
        });
    });
}

$.validator.addMethod("frun", function (value, element) {
    return this.optional(element) || /^0*(\d{1,3}(\.?\d{3})*)\-?([\dkK])$/.test(value);
}, "Debe ingresar un formato de run valido");

$.validator.addMethod("justtext", function (value, element) {
    return this.optional(element) || /^[a-zA-Z ]/.test(value);
}, "Debe ingresar solo texto");

$.validator.addMethod("maxage", function (value, element, rules) {
    date = moment(value,"DD/MM/YYYY");
    age = date.year();
    console.log(value)
    console.log(date)
    console.log(age)
    return  this.optional(element) || age < rules;
},"");

$("#formularioAdopcion").validate({
    rules: {
        email: {
            required: true,
            email: true
        },
        run: {
            required: true,
            frun: true,
            max: 12
        },
        nombre: {
            required: true,
            justtext: true
        },
        fecha_nacimiento: {
            required: true,
            maxage: 2001
        },
        telefono: {
            number: true
        },
        region: {
            required: true,
            min: 1
        },
        ciudad: {
            required: true,
            min: 1
        },
        tipo_vivienda: {
            required: true,
            min: 1
        }
    },
    messages: {
        email: {
            required: "El correo es requerido",
            email: "Debe ingresar un correo electrónico con el formato correcto"
        },
        run: {
            required: "El RUN es requerido",
            frun: "Debe ingresar un RUN válido",
            max: ""
        },
        nombre: {
            required: "El nombre es requerido",
            justtext: "Debe ingresar solo texto"
        },
        fecha_nacimiento: {
            maxage:  "Año debe ser menor a " + 2001,
            required: "La fecha de nacimiento es requerida",
        },
        telefono: {
            number: "Debe ingresar solo números"
        },
        region: {
            required: "La región es requerido",
            min: "Debe seleccionar un una opción"
        },
        ciudad: {
            required: "La ciudad es requerida",
            min: "Debe seleccionar un una opción"
        },
        tipo_vivienda: {
            required: "El tipo de vivienda es requerido",
            min: "Debe seleccionar un una opción"
        }
    }
});