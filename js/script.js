$(function () {


    $(".gallery img").on("click", function (e) {
        var item = e.target;
        var info = $(item).data("info");
        var src = $(item).attr("src");
        var alt = $(item).attr("alt");
        var image = $(".modal-image").find("img");
        var description = $(".modal-image").find("p");

        $(image).attr("src", src);
        $(image).attr("alt", alt);
        $(description).html(info);
    });


    $('#fecha_nacimiento').daterangepicker({
        "singleDatePicker": true,
        
    "showDropdowns": true,
        "locale": {
            "format": "DD/MM/YYYY",
            "separator": " / ",
            "applyLabel": "Aceptar",
            "cancelLabel": "Cancelar",
            "weekLabel": "S",
            "daysOfWeek": [
                "Dom",
                "Lun",
                "Mar",
                "Mir",
                "Jue",
                "Vie",
                "Sab"
            ],
            "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
            ]
        }
    });
    $('#fecha_nacimiento').on("apply.daterangepicker",function(){
        $(this).validate();
    })

});